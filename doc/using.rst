
Using X-SOCS
============

How to use *X-SOCS* to reduce KMAP data, step by step.

.. seealso::
    You can have a look at the :ref:`video-tutorials` as well.

.. toctree::
    :numbered: 1
    :maxdepth: 2

    usage/starting.rst
    usage/project.rst
    usage/project_view.rst
    usage/intensity_view.rst
    usage/conversion.rst
    usage/qspace_view.rst
    usage/results_view.rst


References
----------

X-SOCS can be referred by its DOI on Zenodo: `10.5281/zenodo.10777448 <https://doi.org/10.5281/zenodo.10777448>`_.

Reference publication:

Chahine, G. A., Richard, M.-I., Homs-Regojo, R. A., Tran-Caliste, T. N., Carbone, D., Jacques, V. L. R., Grifone, R., Boesecke, P., Katzer, J., Costina, I., Djazouli, H., Schroeder, T. & Schulli, T. U. *"Imaging of strain and lattice orientation by quick scanning X-ray microscopy combined with three-dimensional reciprocal space mapping."* `J. Appl. Cryst. (2014) 47, 762-769. <https://doi.org/10.1107/S1600576714004506>`_
